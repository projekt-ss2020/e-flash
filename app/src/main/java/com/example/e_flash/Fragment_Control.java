package com.example.e_flash;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.SensorManager;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.text.DecimalFormat;
import java.util.Timer;
import java.util.TimerTask;

public class Fragment_Control extends Fragment {

    private int Speed = 0;
    private boolean stateleft = false;
    private boolean stateright = false;
    private boolean blinkingLeft = false;
    private boolean blinkingRight = false;
    private ImageView blinkleft = null;
    private ImageView blinkright = null;
    private TextView speed = null;
    private FusedLocationProviderClient locationClient;
    private Location location1 = null;
    private Location location2 = null;
    private double speedValue = 0d;
    private int timestamp1 = 0;
    private int timestamp2 = 0;

    @SuppressLint("MissingPermission")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_control, container, false);

        // Calling des Hupbuttons und des zugehörigen Soundfiles
        ImageButton btnhonk = v.findViewById(R.id.btnhonk);
        final MediaPlayer honk = MediaPlayer.create(getActivity(), R.raw.salami);

        // Hupe
        btnhonk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                honk.start();
            }
        });

        // Calling der Blinksignale
        blinkleft = v.findViewById(R.id.left);
        blinkright = v.findViewById(R.id.right);

        // Calling der BlinkButtons
        Button btnleft = v.findViewById(R.id.btnleft);
        Button btnright = v.findViewById(R.id.btnright);

        // Blinken aktivieren
        // Rechts
        btnright.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stateright = !stateright;
                blinkRight();
            }
        });
        // Links
        btnleft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stateleft = !stateleft;
                blinkLeft();
            }
        });

        // Calling Geschwindigkeitsanzeige
        speed = v.findViewById(R.id.speed);

        locationClient = LocationServices.getFusedLocationProviderClient(getActivity());

        // Standort auslesen , Distanzen auslesen und Geschwindigkeit berechnen
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(2 * 500);

        LocationCallback locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                location1 = location2;
                location2 = locationResult.getLastLocation();
                timestamp1 = timestamp2;
                timestamp2 = (int) System.currentTimeMillis();
                getSpeed();
                DecimalFormat df = new DecimalFormat("#0.00");

            }
        };
        locationClient.requestLocationUpdates(locationRequest, locationCallback, null);


        return v;
    }
    //Rechts Blinken
    public void blinkRight() {
        Thread r = new Thread(new Runnable() {
            @Override
            public void run() {
                Timer timer_r = new Timer();
                timer_r.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (stateright) {
                            if (blinkingRight) {
                                blinkright.setImageDrawable(getResources().getDrawable(R.drawable.pfeil_rechts));
                                blinkingRight = false;
                            } else {
                                blinkright.setImageDrawable(getResources().getDrawable(R.drawable.pfeil_rechts_an));
                                blinkingRight = true;
                            }
                        } else {
                            blinkright.setImageDrawable(getResources().getDrawable(R.drawable.pfeil_rechts));
                        }
                    }
                }, 0, 700);
            }
        });
        r.start();
    }
    //Links Blinken
    public void blinkLeft() {
        Thread l = new Thread(new Runnable() {
            @Override
            public void run() {
                Timer timer_l = new Timer();
                timer_l.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (stateleft) {
                            if (blinkingLeft) {
                                blinkleft.setImageDrawable(getResources().getDrawable(R.drawable.pfeil_links));
                                blinkingLeft = false;
                            } else {
                                blinkleft.setImageDrawable(getResources().getDrawable(R.drawable.pfeil_links_an));
                                blinkingLeft = true;
                            }
                        } else {
                            blinkleft.setImageDrawable(getResources().getDrawable(R.drawable.pfeil_links));
                        }
                    }
                }, 0, 700);
            }
        });
        l.start();
    }

    public float getDistance(Location current, Location past) {
        return current.distanceTo(past);
    }

    public void getSpeed() {
        if (location1 != null && location2 != null) {
            speedValue = (getDistance(location2, location1) / (float) ((timestamp2 - timestamp1) / 1000)) * 3.6;
        }
    }

}
