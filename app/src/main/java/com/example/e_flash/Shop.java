package com.example.e_flash;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class Shop extends AppCompatActivity {

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shop);

        // Calling der Toolbar
        Toolbar toolbar = findViewById(R.id.toolbarshop);
        setSupportActionBar(toolbar);

        // Scrollbarkeit herstellen
        recyclerView = findViewById(R.id.recyclerview);
        // Inhalt
        Integer [] bilder;
        bilder = new Integer[]{
                R.drawable.screenshot3, R.drawable.screenshot1, R.drawable.screenshot4, R.drawable.screenshot2};
        RecyclerViewAdapter helpadapter = new RecyclerViewAdapter(bilder);
        recyclerView.setAdapter(helpadapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    // Implementierung des Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        return true;
    }
    // Ansteuerung der Menupunkte
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                Intent intent1 = new Intent(this, MainActivity.class);
                startActivity(intent1);
                return true;
            case R.id.control:
                Intent intent2 = new Intent(this, Controlpanel.class);
                startActivity(intent2);
                return true;
            case R.id.shop:
                Intent intent3 = new Intent(this, Shop.class);
                startActivity(intent3);
                return true;
            case R.id.social:
                Intent intent4 = new Intent(this, Social.class);
                startActivity(intent4);
                return true;
            case R.id.help:
                Intent intent5 = new Intent(this, Help.class);
                startActivity(intent5);
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
