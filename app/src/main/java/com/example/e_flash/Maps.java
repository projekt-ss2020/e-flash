package com.example.e_flash;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class Maps extends Fragment {
    MapView mMapView;
    private GoogleMap googleMap;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_maps, container, false);

        mMapView = (MapView) v.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @SuppressLint("MissingPermission")
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // For showing a move to my location button
                googleMap.setMyLocationEnabled(true);

                // For dropping a marker at a point on the Map

                BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.maplogo);
                LatLng Headquarters = new LatLng(48.767028, 11.432591);
                googleMap.addMarker(new MarkerOptions()
                        .position(Headquarters)
                        .title("Headquarters")
                        .snippet("Here you can find our E-Flash Team!")
                        .icon(icon));

                LatLng SkateparkIngolstadt1 = new LatLng(48.760915,11.419007);
                googleMap.addMarker(new MarkerOptions()
                        .position(SkateparkIngolstadt1)
                        .title("Skatepark")
                        .snippet("Meet other skaters here!")
                        .icon(icon));

                LatLng SkateparkIngolstadt2 = new LatLng(48.780392,11.400092);
                googleMap.addMarker(new MarkerOptions()
                        .position(SkateparkIngolstadt2)
                        .title("Skatepark")
                        .snippet("Meet other skaters here!")
                        .icon(icon));

                LatLng SkateparkIngolstadt3 = new LatLng(48.755653,11.421797);
                googleMap.addMarker(new MarkerOptions()
                        .position(SkateparkIngolstadt3)
                        .title("Skatepark")
                        .snippet("Meet other skaters here!")
                        .icon(icon));

                LatLng SkateparkIngolstadt4 = new LatLng(48.784698,11.444069);
                googleMap.addMarker(new MarkerOptions()
                        .position(SkateparkIngolstadt4)
                        .title("Skatepark")
                        .snippet("Meet other skaters here!")
                        .icon(icon));

            }


        });

        return v;
    }
    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}
