package com.example.e_flash;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.ActionBar;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Calling der Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Fragen nach Locationpermission
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
        }
    }

    // Implementierung des Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        return true;
    }
    // Ansteuerung der Menupunkte
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                Intent intent1 = new Intent(this, MainActivity.class);
                startActivity(intent1);
                return true;
            case R.id.control:
                Intent intent2 = new Intent(this, Controlpanel.class);
                startActivity(intent2);
                return true;
            case R.id.shop:
                Intent intent3 = new Intent(this, Shop.class);
                startActivity(intent3);
                return true;
            case R.id.social:
                Intent intent4 = new Intent(this, Social.class);
                startActivity(intent4);
                return true;
            case R.id.help:
                Intent intent5 = new Intent(this, Help.class);
                startActivity(intent5);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Aufrufen des Controlpanel über Button
    public void sendMessage(View view) {

        Intent intent = new Intent(this, Controlpanel.class);
        startActivity(intent);

    }

    //Aufrufen des Shop über Button
    public void sendMessage2(View view) {

        Intent intent = new Intent(this, Shop.class);
        startActivity(intent);

    }

    //Aufrufen des Social über Button
    public void sendMessage3(View view) {

        Intent intent = new Intent(this, Social.class);
        startActivity(intent);

    }

    //Aufrufen der Hilfe über Button
    public void sendMessage4(View view) {

        Intent intent = new Intent(this, Help.class);
        startActivity(intent);

    }


}
