package com.example.e_flash;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.HelpviewHolder> {

    /*String [] mtitles;
    String [] mhelptext;*/
    Integer [] mbilder;

    public RecyclerViewAdapter(/*String [] mhelptext, String [] mtitles*/ Integer[] mbilder) {
        /*this.mhelptext = mhelptext;
        this.mtitles = mtitles;*/
        this.mbilder = mbilder;
    }

    @NonNull
    @Override
    public HelpviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View HelpView = inflater.inflate(R.layout.help_item, parent, false);
        HelpviewHolder viewHolder = new HelpviewHolder(HelpView);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull HelpviewHolder holder, int position) {
        /*String currenthelptext = mhelptext[position];
        String currenttitle = mtitles[position];*/
        Integer currentbild = mbilder[position];

        /*holder.helptext.setText(currenthelptext);
        holder.titles.setText(currenttitle);*/
        holder.imageView.setImageResource(currentbild);
    }

    @Override
    public int getItemCount() {
        return mbilder.length;
    }

    public class HelpviewHolder extends RecyclerView.ViewHolder {
        /*TextView helptext;
        TextView titles;*/
        ImageView imageView;

        public HelpviewHolder(@NonNull View itemView) {
            super(itemView);
            /*helptext = itemView.findViewById(R.id.helptext);
            titles = itemView.findViewById(R.id.titles);*/
            imageView = itemView.findViewById(R.id.imageView);
        }
    }
}
