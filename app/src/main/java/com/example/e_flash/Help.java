package com.example.e_flash;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class Help extends AppCompatActivity {

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help);

        // Calling der Toolbar
        Toolbar toolbar = findViewById(R.id.toolbarhelp);
        setSupportActionBar(toolbar);

        // Scrollbarkeit herstellen
        recyclerView = findViewById(R.id.recyclerview);
        // Inhalt der Hilfe
        /*String [] helptext;
        helptext = new String[]{
                "Im Controlpanel müssen Sie zunächst den persönlichen Code, der mit ihrem Longboard geliefert wurde, eingeben. Dann finden Sie:",
                "Ansteuerung der Blinker links und rechts, bei Aktivierung blinken auch die Symbolbilder",
                "Beschleunigt auf 10 km/h",
                "Bremst auf 0 km/h",
                "Regulieren der Geschwindigkeit um jeweils 2 km/h",
                "Berechnet Ihre aktuelle Geschwindigkeit aus Ihren GPS-Daten (wenn nicht bereits geschehen, dann erlauben Sie dies bitte in Ihren Einstellungen)"
        };
        String [] titles;
        titles = new String[]{
                "1. Control:",
                "Button Left / Right:",
                "Button Start:",
                "Button Stop:",
                "Button Faster / Slower:",
                "Speedmeter:"
        };*/

    Integer [] bilder;
    bilder = new Integer[]{
                R.drawable.screenunlock,
                R.drawable.screencontrol,
                R.drawable.screensocial};


    RecyclerViewAdapter helpadapter = new RecyclerViewAdapter(bilder);
        recyclerView.setAdapter(helpadapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    };

    // Implementierung des Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        return true;
    }
    // Ansteuerung der Menupunkte
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                Intent intent1 = new Intent(this, MainActivity.class);
                startActivity(intent1);
                return true;
            case R.id.control:
                Intent intent2 = new Intent(this, Controlpanel.class);
                startActivity(intent2);
                return true;
            case R.id.shop:
                Intent intent3 = new Intent(this, Shop.class);
                startActivity(intent3);
                return true;
            case R.id.social:
                Intent intent4 = new Intent(this, Social.class);
                startActivity(intent4);
                return true;
            case R.id.help:
                Intent intent5 = new Intent(this, Help.class);
                startActivity(intent5);
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
