package com.example.e_flash;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;


public class Social extends AppCompatActivity{
    private SectionsStatePagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.social);

        // Calling der Toolbar
        Toolbar toolbar = findViewById(R.id.toolbarsocial);
        setSupportActionBar(toolbar);

        //Calling des ViewPager für die Fragments
        mSectionsPagerAdapter = new SectionsStatePagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(mViewPager);
    }

    // Implementierung des Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        return true;
    }
    // Ansteuerung der Menupunkte
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                Intent intent1 = new Intent(this, MainActivity.class);
                startActivity(intent1);
                return true;
            case R.id.control:
                Intent intent2 = new Intent(this, Controlpanel.class);
                startActivity(intent2);
                return true;
            case R.id.shop:
                Intent intent3 = new Intent(this, Shop.class);
                startActivity(intent3);
                return true;
            case R.id.social:
                Intent intent4 = new Intent(this, Social.class);
                startActivity(intent4);
                return true;
            case R.id.help:
                Intent intent5 = new Intent(this, Help.class);
                startActivity(intent5);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupViewPager(ViewPager viewPager){
        SectionsStatePagerAdapter adapter = new SectionsStatePagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Maps(), "Maps");
        viewPager.setAdapter(adapter);
    }



}